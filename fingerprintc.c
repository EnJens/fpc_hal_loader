#include <cutils/log.h>

#include <stdint.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <dlfcn.h>

#include <hardware/hardware.h>
#include <hardware/fingerprint.h>

#define DBGPRINT(...) do { printf(__VA_ARGS__); fflush(stdout);} while(0)

static int load(const char *id,
        const char *path,
        const struct hw_module_t **pHmi)
{
    int status = -EINVAL;
    void *handle = NULL;
    struct hw_module_t *hmi = NULL;

    DBGPRINT("Loading module id %s from %s\n", id, path);
    /*
     * load the symbols resolving undefined symbols before
     * dlopen returns. Since RTLD_GLOBAL is not or'd in with
     * RTLD_NOW the external symbols will not be global
     */
    handle = dlopen(path, RTLD_NOW);
    if (handle == NULL) {
        char const *err_str = dlerror();
        ALOGE("load: module=%s\n%s", path, err_str?err_str:"unknown");
        status = -EINVAL;
        goto done;
    }
    DBGPRINT("Loaded module at %p\n", handle);

    /* Get the address of the struct hal_module_info. */
    const char *sym = HAL_MODULE_INFO_SYM_AS_STR;
    hmi = (struct hw_module_t *)dlsym(handle, sym);
    if (hmi == NULL) {
        ALOGE("load: couldn't find symbol %s", sym);
        status = -EINVAL;
        goto done;
    }
    DBGPRINT("Got hmi symbol at %p\n", hmi);

    /* Check that the id matches */
    if (strcmp(id, hmi->id) != 0) {
        ALOGE("load: id=%s != hmi->id=%s", id, hmi->id);
        status = -EINVAL;
        goto done;
    }

    hmi->dso = handle;

    /* success */
    status = 0;

    done:
    if (status != 0) {
        hmi = NULL;
        if (handle != NULL) {
            dlclose(handle);
            handle = NULL;
        }
    } else {
        ALOGV("loaded HAL id=%s path=%s hmi=%p handle=%p",
                id, path, *pHmi, handle);
    }

    *pHmi = hmi;

    return status;
}


void hal_notify_callback(const fingerprint_msg_t *msg)
{
    DBGPRINT("Got fingerprint msg callback: type=%d\n", msg->type);
    switch(msg->type)
    {
        case FINGERPRINT_ERROR:
            DBGPRINT("Fingerprint error event: %d\n", msg->data.error);
            break;
        case FINGERPRINT_ACQUIRED:
            DBGPRINT("Fingerprint acquired: %d\n", msg->data.acquired.acquired_info);
            break;
        case FINGERPRINT_TEMPLATE_ENROLLING:
            DBGPRINT("Fingerprint template enrolling\n");
            break;
        case FINGERPRINT_TEMPLATE_REMOVED:
            DBGPRINT("Fingerprint template removed\n");
            break;
        case FINGERPRINT_AUTHENTICATED:
            DBGPRINT("Fingerprint authenticated: (fid=%d, gid=%d)\n", msg->data.authenticated.finger.fid, msg->data.authenticated.finger.gid);
            break;
        default:
            DBGPRINT("Fingeprint unknown message\n");
            break;
    }
}

int main(int argc, char**argv)
{
    const hw_module_t *hw_module = NULL;

    if(argc < 2)
    {
        printf("Usage: %s </path/to/fingerprint.<hwid>.so>\n", argv[0]);
        return -1;
    }
    DBGPRINT("Loading module\n");
    if(load(FINGERPRINT_HARDWARE_MODULE_ID, argv[1], &hw_module) != 0 || hw_module == NULL)
    {
        fprintf(stderr, "Unable to loadfingerprint hal\n");
        return -2;
    }
    const fingerprint_module_t* fpc_module = (const fingerprint_module_t*)hw_module;
    hw_device_t *device = NULL;

    if(fpc_module->common.methods->open == NULL)
    {
        DBGPRINT("Missing open method!?\n");
        return -3;
    }
    DBGPRINT("Calling open\n");
    if(fpc_module->common.methods->open(hw_module, NULL, &device) != 0 || device == NULL)
    {
        fprintf(stderr, "Unable to open() module\n");
        return -4;
    }
    DBGPRINT("After open, device=%p\n", device);
    DBGPRINT("Version: %d\n", device->version);

    fingerprint_device_t *fpc_device = (fingerprint_device_t*)device;
    int ret = fpc_device->set_notify(fpc_device, hal_notify_callback);
    if(ret < 0) {
        DBGPRINT("Failed to set_notify(%p): err=%d\n", hal_notify_callback, ret);
        return -5;
    }

    DBGPRINT("Fingerprint hal successfully initialized\n");

    ret = fpc_device->set_active_group(fpc_device, 0, "/data/system/users/0/fpdata");
    DBGPRINT("Set active group result: %d\n", ret);

    ret = fpc_device->authenticate(fpc_device, 0, 0);
    DBGPRINT("Authenticate result: %d\n", ret);
/*    uint64_t preenrollid = fpc_device->pre_enroll(fpc_device);
    DBGPRINT("pre-enroll response: %llu, %p\n", preenrollid, preenrollid);*/

    while(1)
    {
    };
    return 0;
}


LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    fingerprintc.c

LOCAL_SHARED_LIBRARIES := \
    liblog \
    libcutils \
    libhardware \
    libdl

LOCAL_MODULE:= fingerprintc
LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES += 

include $(BUILD_EXECUTABLE)

